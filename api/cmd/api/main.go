package main

import (
	"api"
	"api/database/mysql"
	"api/http"
	"fmt"
	"log"
	netHttp "net/http"

	"github.com/joho/godotenv"
	"github.com/julienschmidt/httprouter"
)

func main() {
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}

	auth := http.NewJWTAuth("verysecretkey")
	guard := http.NewAuthGuard(auth)

	service := api.UserService{
		mysql.NewUserRepository(mysql.NewConnection()),
		auth,
		api.SMTPMailer{},
	}

	h := http.Handler{service}

	r := httprouter.New()
	r.POST("/api/login", h.Login)
	r.GET("/api/login/google", h.LoginGoogle)
	r.POST("/api/signup", h.Signup)
	r.GET("/api/user", guard(h.User))
	r.POST("/api/user/update", guard(h.UpdateUser))
	r.GET("/api/user/refresh-password", h.EmailRefresh)
	r.POST("/api/user/password-update", h.UpdatePassword)

	fmt.Println("API server running on port 3000")
	netHttp.ListenAndServe(":3000", r)

}
