package api

import (
	"fmt"
	"net/smtp"
	"os"
)

type Mailer interface {
	Send(to string, body string) error
}

type SMTPMailer struct {
}

func (m SMTPMailer) Send(to string, body string) error {
	from := os.Getenv("SMTP_FROM")
	pass := os.Getenv("SMTP_PASS")
	host := os.Getenv("SMTP_HOST")
	port := os.Getenv("SMTP_PORT")

	msg := "From: " + from + "\n" +
		"To: " + to + "\n" +
		"Subject: Refresh password\n\n" +
		body

	auth := smtp.PlainAuth("", from, pass, host)

	return smtp.SendMail(fmt.Sprintf("%s:%s", host, port), auth, from, []string{to}, []byte(msg))
}
