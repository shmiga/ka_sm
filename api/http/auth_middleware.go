package http

import (
	"api"
	"context"
	"fmt"
	"net/http"
	"strings"

	"github.com/julienschmidt/httprouter"
)

func NewAuthGuard(auth api.AuthService) func(httprouter.Handle) httprouter.Handle {
	return func(h httprouter.Handle) httprouter.Handle {
		return func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
			token, err := parseJwtHeader(r)
			if err != nil {
				responseInternalError(w, err)
				return
			}

			if valid, userID := auth.Valid(token); valid {
				ctx := context.WithValue(r.Context(), "userID", userID)
				h(w, r.WithContext(ctx), ps)
			} else {
				responseUnauthorized(w)
			}
		}
	}
}

func parseJwtHeader(r *http.Request) (string, error) {
	fields := strings.Fields(r.Header.Get("Authorization"))
	if len(fields) > 1 && fields[1] != "null" {
		return fields[1], nil
	}
	return "", fmt.Errorf("authorization header invalid")
}
