package http

import (
	jwt "github.com/dgrijalva/jwt-go"
)

type Claims struct {
	UserID int64 `json:"userID"`
	jwt.StandardClaims
}

// JWTAuth uses jwt token for authentification
type JWTAuth struct {
	Secret string
}

// Valid returns if auth token is valid
func (a *JWTAuth) Valid(tokenString string) (bool, int64) {
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		return []byte(a.Secret), nil
	})
	if err != nil {
		return false, 0
	}

	claims := token.Claims.(jwt.MapClaims)

	if userID, ok := claims["userID"].(float64); !ok {
		return false, 0
	} else {
		return true, int64(userID)
	}
}

// Sign returns new token
func (a *JWTAuth) Sign(ID int64) (string, error) {
	claims := Claims{
		ID,
		jwt.StandardClaims{
			Issuer: "example",
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	return token.SignedString([]byte(a.Secret))
}

func NewJWTAuth(secret string) *JWTAuth {
	return &JWTAuth{secret}
}
