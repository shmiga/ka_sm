package http

import (
	"api"
	"encoding/json"
	"net/http"

	"github.com/davecgh/go-spew/spew"
	"github.com/julienschmidt/httprouter"
)

type Handler struct {
	UserService api.UserService
}

func (h *Handler) Login(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	request := &api.LoginUserRequest{}
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(request)
	if err != nil {
		responseUnprocessable(w, r, err)
		return
	}

	_, token, err := h.UserService.Login(request)
	if err != nil {
		responseUnprocessable(w, r, err)
		return
	}

	responseJSON(w, r, token)
}

func (h *Handler) Signup(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	request := &api.StoreUserRequest{}
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(request)
	if err != nil {
		responseUnprocessable(w, r, err)
		return
	}

	if ok, err := request.Valid(); !ok {
		responseUnprocessable(w, r, err)
		return
	}

	user, token, err := h.UserService.Signup(request)
	if err != nil {
		responseUnprocessable(w, r, err)
		return
	}

	responseJSON(w, r, map[string]interface{}{
		"user":  user,
		"token": token,
	})
}

func (h *Handler) UpdateUser(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	request := &api.UpdateUserRequest{}
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(request)
	if err != nil {
		responseUnprocessable(w, r, err)
		return
	}

	userID := r.Context().Value("userID").(int64)
	request.ID = userID

	ok, err := request.Valid()
	if !ok {
		responseUnprocessable(w, r, err)
		return
	}

	user, err := h.UserService.Update(request)
	if err != nil {
		responseUnprocessable(w, r, err)
		return
	}

	responseJSON(w, r, user)
}

func (h *Handler) EmailRefresh(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	email := r.URL.Query().Get("email")

	err := h.UserService.RefreshPassword(email)
	if err != nil {
		responseUnprocessable(w, r, err)
		return
	}
	responseJSON(w, r, "Refresh link sent")
}

func (h *Handler) UpdatePassword(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	request := &api.UpdatePasswordRequest{}
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(request)
	if err != nil {
		responseUnprocessable(w, r, err)
		return
	}

	if ok, err := request.Valid(); !ok {
		responseUnprocessable(w, r, err)
		return
	}

	err = h.UserService.UpdatePassword(request)
	if err != nil {
		responseUnprocessable(w, r, err)
		return
	}

	responseJSON(w, r, "Success")
}

func (h *Handler) User(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	userID := r.Context().Value("userID").(int64)

	user, err := h.UserService.GetUserBy("id", userID)
	if err != nil {
		responseUnprocessable(w, r, err)
		return
	}
	responseJSON(w, r, user)
}

func (h *Handler) LoginGoogle(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	id := r.URL.Query().Get("id")
	_, token, err := h.UserService.LoginGoogle(id)
	spew.Dump(token, err)
	if err != nil {
		responseUnprocessable(w, r, err)
		return
	}

	responseJSON(w, r, token)
}
