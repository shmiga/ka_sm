package http

import (
	"encoding/json"
	"net/http"
)

func responseJSON(w http.ResponseWriter, r *http.Request, data interface{}) {
	response, err := json.Marshal(map[string]interface{}{
		"success": true,
		"data":    data,
	})

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(response)
}

func responseUnprocessable(w http.ResponseWriter, r *http.Request, err error) {
	response, err := json.Marshal(map[string]interface{}{
		"success": false,
		"error":   err.Error(),
	})

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusUnprocessableEntity)
	w.Header().Set("Content-Type", "application/json")
	w.Write(response)
}

func responseUnauthorized(w http.ResponseWriter) {
	response, _ := json.Marshal(map[string]interface{}{
		"success": false,
		"error":   "Unauthorized",
	})

	w.WriteHeader(http.StatusUnauthorized)
	w.Header().Set("Content-Type", "application/json")
	w.Write(response)
}

func responseInternalError(w http.ResponseWriter, err error) {
	response, _ := json.Marshal(map[string]interface{}{
		"success": false,
		"error":   err.Error(),
	})

	w.WriteHeader(http.StatusInternalServerError)
	w.Header().Set("Content-Type", "application/json")
	w.Write(response)
}
