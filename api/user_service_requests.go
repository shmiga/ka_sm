package api

import "github.com/asaskevich/govalidator"

type StoreUserRequest struct {
	Email    string `json:"email" valid:"required,email"`
	Name     string `json:"name"`
	Password string `json:"password" valid:"required"`
	GoogleID string `json:"googleID"`
}

func (r *StoreUserRequest) Valid() (bool, error) {
	return govalidator.ValidateStruct(r)
}

type UpdateUserRequest struct {
	ID        int64  `json:"id"`
	FullName  string `json:"fullName" db:"full_name"`
	Email     string `json:"email" db:"email" valid:"required"`
	Telephone string `json:"telephone" db:"telephone"`
	Address   string `json:"address" db:"address"`
}

func (r *UpdateUserRequest) Valid() (bool, error) {
	return govalidator.ValidateStruct(r)
}

type LoginUserRequest struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

type UpdatePasswordRequest struct {
	Token           string `json:"token" valid:"required"`
	Password        string `json:"password" valid:"required"`
	PasswordConfirm string `json:"passwordConfirm"required`
}

func (r *UpdatePasswordRequest) Valid() (bool, error) {
	return govalidator.ValidateStruct(r)
}

type ShowUserRequest struct {
	Token string `json:"token" valid:"required"`
}

func (r *ShowUserRequest) Valid() (bool, error) {
	return govalidator.ValidateStruct(r)
}
