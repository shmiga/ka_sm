package api

// Auth implements methods necessary for authenitfication
type AuthService interface {
	Valid(string) (bool, int64)
	Sign(id int64) (string, error)
}
