package mysql

import (
	"api"
	"fmt"

	"github.com/jmoiron/sqlx"
)

type UserRepository struct {
	db *sqlx.DB
}

func (r *UserRepository) GetBy(key string, value interface{}) (*api.User, error) {
	user := &api.User{}

	err := r.db.QueryRowx(fmt.Sprintf("SELECT * FROM users WHERE %s = ?", key), value).StructScan(user)
	if err != nil {
		return nil, err
	}

	return user, nil
}

func (r *UserRepository) Store(user *api.User) (*api.User, error) {
	res, err := r.db.Exec("INSERT INTO users (full_name, email, telephone, address, password, google_id, created_at) values(?, ?, ?, ?, ?, ?, ?)",
		user.FullName, user.Email, user.Telephone, user.Address, user.Password, user.GoogleID, user.CreatedAt)

	if err != nil {
		return nil, err
	}

	id, err := res.LastInsertId()
	if err != nil {
		return nil, err
	}

	user.ID = id
	return user, nil
}

func (r *UserRepository) Update(user *api.User) error {
	_, err := r.db.Exec(`
		UPDATE users SET
		full_name = ?,
		email = ?,
		telephone = ?,
		address = ?,
		refresh_token = ?
		WHERE id = ?
	`, user.FullName, user.Email, user.Telephone, user.Address, user.RefreshToken, user.ID)

	return err
}

func (r *UserRepository) UpdatePassword(pass string, token string) error {
	res, err := r.db.Exec(`
		UPDATE users set password = ?, refresh_token = NULL WHERE refresh_token = ?
	`, pass, token)

	rows, err := res.RowsAffected()
	if err != nil {
		return err
	}

	if rows == 0 {
		return api.ErrTokenNotValid
	}

	return err
}

func NewUserRepository(db *sqlx.DB) *UserRepository {
	return &UserRepository{db}
}
