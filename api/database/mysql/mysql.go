package mysql

import (
	"log"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
)

// NewConnection returns new pointer to mysql connection pool
func NewConnection() *sqlx.DB {
	conn, err := sqlx.Connect("mysql", "root:pass@(localhost:3306)/ka_sm?parseTime=true")
	if err != nil {
		log.Fatal("Cannot connect to database")
	}
	return conn
}
