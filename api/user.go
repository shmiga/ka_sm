package api

import (
	"errors"
	"time"
)

// Error definitions
var (
	ErrInvalidCredentials = errors.New("Invalid credentials")
	ErrEmailExists        = errors.New("Email already exists")
	ErrUserNotFound       = errors.New("User not found")
	ErrPasswordMismatch   = errors.New("Password confirmation doesn`t match password")
	ErrTokenNotValid      = errors.New("Refresh token not valid")
)

type User struct {
	ID           int64
	FullName     string     `json:"fullName" db:"full_name"`
	Email        string     `json:"email" db:"email"`
	Telephone    *string    `json:"telephone" telephone:"telephone"`
	Address      *string    `json:"address" db:"address"`
	Password     string     `json:"-" db:"password"`
	RefreshToken *string    `json:"-" db:"refresh_token"`
	GoogleID     *string    `json:"-" db:"google_id"`
	CreatedAt    *time.Time `json:"createdAt" db:"created_at"`
}

type UserRepository interface {
	Store(*User) (*User, error)
	GetBy(string, interface{}) (*User, error)
	Update(*User) error
	UpdatePassword(string, string) error
}
