package api

import (
	"crypto/rand"
	"encoding/base64"
	"fmt"
	"os"
	"time"

	"golang.org/x/crypto/bcrypt"
)

type UserService struct {
	UserRepository UserRepository
	Auth           AuthService
	Mailer         Mailer
}

func (s *UserService) Login(req *LoginUserRequest) (*User, string, error) {
	user, err := s.UserRepository.GetBy("email", req.Email)
	if err != nil {
		return nil, "", ErrInvalidCredentials
	}

	err = bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(req.Password))
	if err != nil {
		return nil, "", ErrInvalidCredentials
	}

	token, err := s.Auth.Sign(user.ID)
	if err != nil {
		return nil, "", nil
	}

	return user, token, nil
}

func (s *UserService) LoginGoogle(id string) (*User, string, error) {
	user, err := s.UserRepository.GetBy("google_id", id)
	if err != nil {
		return nil, "", ErrInvalidCredentials
	}

	token, err := s.Auth.Sign(user.ID)
	if err != nil {
		return nil, "", nil
	}

	return user, token, nil
}

func (s *UserService) Signup(req *StoreUserRequest) (*User, string, error) {
	existing, _ := s.UserRepository.GetBy("email", req.Email)
	if existing != nil {
		return nil, "", ErrEmailExists
	}

	hash, err := bcrypt.GenerateFromPassword([]byte(req.Password), bcrypt.DefaultCost)
	if err != nil {
		return nil, "", err
	}

	now := time.Now()

	u := &User{
		Email:     req.Email,
		FullName:  req.Name,
		GoogleID:  &req.GoogleID,
		Password:  string(hash),
		CreatedAt: &now,
	}

	user, err := s.UserRepository.Store(u)
	if err != nil {
		return nil, "", err
	}

	token, err := s.Auth.Sign(user.ID)
	if err != nil {
		return nil, "", nil
	}

	return user, token, nil
}

func (s *UserService) Update(req *UpdateUserRequest) (*User, error) {
	user := &User{
		ID:        req.ID,
		FullName:  req.FullName,
		Email:     req.Email,
		Telephone: &req.Telephone,
		Address:   &req.Address,
	}

	err := s.UserRepository.Update(user)
	if err != nil {
		return nil, err
	}

	user, err = s.UserRepository.GetBy("id", user.ID)
	if err != nil {
		return nil, err
	}

	return user, nil

}

func (s *UserService) RefreshPassword(email string) error {
	user, err := s.UserRepository.GetBy("email", email)
	if err != nil {
		return ErrUserNotFound
	}

	token, err := s.generateRandomString(128)
	if err != nil {
		return err
	}

	user.RefreshToken = &token
	err = s.UserRepository.Update(user)
	if err != nil {
		return err
	}

	return s.Mailer.Send(user.Email, fmt.Sprintf("To refresh your password click link: \n %s?token=%s", os.Getenv("APP_URL"), token))
}

func (s *UserService) UpdatePassword(req *UpdatePasswordRequest) error {
	if req.Password != req.PasswordConfirm {
		return ErrPasswordMismatch
	}

	hash, err := bcrypt.GenerateFromPassword([]byte(req.Password), bcrypt.DefaultCost)
	if err != nil {
		return err
	}

	return s.UserRepository.UpdatePassword(string(hash), req.Token)
}

func (s *UserService) GetUserBy(key string, value interface{}) (*User, error) {
	return s.UserRepository.GetBy(key, value)
}

func (s *UserService) generateRandomString(length int) (string, error) {
	b := make([]byte, length)
	_, err := rand.Read(b)
	if err != nil {
		return "", err
	}
	return base64.URLEncoding.EncodeToString(b), nil
}
