package http

import (
	"bytes"
	"fmt"
	"html/template"
	"net/http"
	"os"
)

// Renderer handles incoming http requests
type Renderer struct {
	session  SessionHandler
	template *template.Template
}

func (h *Renderer) WithCookie(w http.ResponseWriter, r *http.Request, key string, value string) *Renderer {
	h.session.Set(r, w, key, value)
	return h
}

func (h *Renderer) Flash(w http.ResponseWriter, r *http.Request, msg interface{}) {
	h.session.Flash(r, w, msg)
}

func (h *Renderer) View(w http.ResponseWriter, r *http.Request, tpl string, data map[string]interface{}) {
	auth, err := h.session.GetValue(r, "auth")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

	if auth == "" {
		auth = nil
	}

	if data == nil {
		data = make(map[string]interface{})
	}

	data["auth"] = auth
	data["flash"] = h.session.Flashes(r, w)
	data["assetsJS"] = fmt.Sprintf("%s/app.js", os.Getenv("ASSETS_PATH"))
	data["assetsCSS"] = fmt.Sprintf("%s/app.css", os.Getenv("ASSETS_PATH"))

	buf := &bytes.Buffer{}
	err = h.template.ExecuteTemplate(buf, tpl, data)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	} else {
		buf.WriteTo(w)
	}
}

// NewRenderer returns pointer to renderer instance
func NewRenderer(s SessionHandler, t *template.Template) *Renderer {
	return &Renderer{s, t}
}
