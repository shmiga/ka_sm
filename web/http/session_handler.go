package http

import "net/http"

type SessionHandler interface {
	GetValue(*http.Request, string) (interface{}, error)
	Set(*http.Request, http.ResponseWriter, string, string) error
	Flash(*http.Request, http.ResponseWriter, interface{}) error
	Flashes(*http.Request, http.ResponseWriter) interface{}
}
