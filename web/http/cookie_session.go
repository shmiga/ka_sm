package http

import (
	"net/http"

	"github.com/gorilla/sessions"
)

const sessionName = "my-session"

type CookieSession struct {
	store *sessions.CookieStore
}

func (s *CookieSession) GetValue(r *http.Request, key string) (interface{}, error) {
	session, err := s.store.Get(r, sessionName)
	if err != nil {
		return nil, err
	}
	return session.Values[key], nil
}

func (s *CookieSession) Set(r *http.Request, w http.ResponseWriter, key string, value string) error {
	session, err := s.store.Get(r, sessionName)
	if err != nil {
		return err
	}
	session.Values[key] = value
	return session.Save(r, w)
}

func (s *CookieSession) Flashes(r *http.Request, w http.ResponseWriter) interface{} {
	session, err := s.store.Get(r, sessionName)
	if err != nil {
		return err
	}
	flash := session.Flashes()
	session.Save(r, w)
	return flash
}

func (s *CookieSession) Flash(r *http.Request, w http.ResponseWriter, flash interface{}) error {
	session, err := s.store.Get(r, sessionName)
	if err != nil {
		return err
	}
	session.AddFlash(flash)
	return session.Save(r, w)
}

func NewCookieSession(secret []byte) *CookieSession {
	store := sessions.NewCookieStore(secret)
	return &CookieSession{store}
}
