package http

import (
	"context"
	"crypto/rand"
	"encoding/base64"
	"encoding/json"
	"net/http"
	"os"
	"web"

	"github.com/julienschmidt/httprouter"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
)

// Handler groups dependencies needed to handle http requests
type Handler struct {
	Render  *Renderer
	API     web.APIService
	Session SessionHandler
}

func (h *Handler) Index(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	h.Render.View(w, r, "index.gohtml", nil)
}

func (h *Handler) GetLogin(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	h.Render.View(w, r, "login.gohtml", nil)
}

func (h *Handler) PostLogin(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	resp, code, err := h.API.POST("/login", map[string]interface{}{
		"email":    r.PostFormValue("email"),
		"password": r.PostFormValue("password"),
	})
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

	if code == http.StatusUnprocessableEntity {
		h.Session.Flash(r, w, resp["error"])
		http.Redirect(w, r, "/login", http.StatusSeeOther)
		return
	}

	data := resp["data"].(map[string]interface{})
	token := data["token"].(string)

	if code == http.StatusOK {
		h.Session.Set(r, w, "auth", token)
		http.Redirect(w, r, "/profile", http.StatusSeeOther)
		return
	}

	http.Error(w, err.Error(), http.StatusInternalServerError)
}

func (h *Handler) GetSignup(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	h.Render.View(w, r, "signup.gohtml", nil)
}

func (h *Handler) PostSignup(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	if r.PostFormValue("password") != r.PostFormValue("passwordConfirmation") {
		h.Session.Flash(r, w, "Passwords doesnt match")
		http.Redirect(w, r, "/signup", http.StatusSeeOther)
		return
	}

	resp, code, err := h.API.POST("/signup", map[string]interface{}{
		"email":    r.PostFormValue("email"),
		"password": r.PostFormValue("password"),
	})
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

	if code == http.StatusUnprocessableEntity {
		h.Render.Flash(w, r, resp["error"])
		http.Redirect(w, r, "/signup", http.StatusSeeOther)
		return
	}

	data := resp["data"].(map[string]interface{})
	token := data["token"].(string)

	h.Render.WithCookie(w, r, "auth", token).View(w, r, "finish.gohtml", data)

}

func (h *Handler) Logout(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	h.Session.Set(r, w, "auth", "")
	http.Redirect(w, r, "/", http.StatusSeeOther)
}

func (h *Handler) GetProfile(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	token := h.GetToken(r)

	res, _, err := h.API.SetAuth(token).GET("/user")
	if err != nil || token == "" {
		http.Redirect(w, r, "/", http.StatusSeeOther)
		return
	}

	h.Render.View(w, r, "profile.gohtml", res)
}

func (h *Handler) PostProfile(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	token := h.GetToken(r)
	resp, code, err := h.API.SetAuth(token).POST("/user/update", map[string]interface{}{
		"fullName":  r.PostFormValue("fullName"),
		"email":     r.PostFormValue("email"),
		"password":  r.PostFormValue("password"),
		"address":   r.PostFormValue("address"),
		"telephone": r.PostFormValue("telephone"),
	})

	if code == http.StatusUnprocessableEntity {
		h.Session.Flash(r, w, resp["error"])
		http.Redirect(w, r, "/profile", http.StatusSeeOther)
		return
	}

	if code == http.StatusOK {
		h.Render.View(w, r, "profile.gohtml", resp)
		return
	}

	http.Error(w, err.Error(), http.StatusInternalServerError)
}

func (h *Handler) GoogleRedirect(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	user, err := h.handleGoogleCallback(r)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// check if user already signed with google
	if res, code, _ := h.API.GET("/login/google?" + user.ID); code == http.StatusOK {
		token := res["data"].(string)
		h.Session.Set(r, w, "auth", token)
		http.Redirect(w, r, "/profile", http.StatusSeeOther)
		return
	}

	rnd, err := h.generateRandomString(64)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	resp, code, err := h.API.POST("/signup", map[string]interface{}{
		"name":     user.Name,
		"email":    user.Email,
		"password": rnd,
	})

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if code == http.StatusUnprocessableEntity {
		h.Session.Flash(r, w, resp["error"])
		http.Redirect(w, r, "/signup", http.StatusSeeOther)
		return
	}

	data := resp["data"].(map[string]interface{})
	token := data["token"].(string)

	h.Session.Set(r, w, "auth", token)
	h.Render.View(w, r, "finish.gohtml", data)
	return
}

func (h *Handler) RefreshEmail(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	email := r.PostFormValue("email")
	res, code, err := h.API.GET("/user/refresh-password?email=" + email)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if code == http.StatusUnprocessableEntity {
		h.Session.Flash(r, w, res["error"])
		http.Redirect(w, r, "/login", http.StatusSeeOther)
		return
	}
	http.Redirect(w, r, "/login", http.StatusSeeOther)
}

func (h *Handler) GoogleSign(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {

	conf := googleOauthConfig()

	state, err := h.generateRandomString(128)
	if err != nil {
		state = "random"
	}

	h.Session.Set(r, w, "state", state)

	url := conf.AuthCodeURL(state)
	http.Redirect(w, r, url, http.StatusSeeOther)
}

func (h *Handler) generateRandomString(length int) (string, error) {
	b := make([]byte, length)
	_, err := rand.Read(b)
	if err != nil {
		return "", err
	}
	return base64.URLEncoding.EncodeToString(b), nil
}

func (h *Handler) GetToken(r *http.Request) string {
	auth, _ := h.Session.GetValue(r, "auth")
	if auth == nil || auth == "" {
		return ""
	}

	return auth.(string)
}

func (h *Handler) handleGoogleCallback(r *http.Request) (*web.GoogleUser, error) {
	state, err := h.Session.GetValue(r, "state")
	if err != nil {
		return nil, err
	}

	if r.FormValue("state") != state {
		return nil, err
	}

	conf := googleOauthConfig()

	token, err := conf.Exchange(context.Background(), r.FormValue("code"))
	if err != nil {
		return nil, err
	}

	resp, err := http.Get("https://www.googleapis.com/oauth2/v2/userinfo?access_token=" + token.AccessToken)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	user := web.GoogleUser{}
	err = json.NewDecoder(resp.Body).Decode(&user)
	if err != nil {
		return nil, err
	}
	return &user, nil
}

func googleOauthConfig() *oauth2.Config {
	return &oauth2.Config{
		ClientID:     os.Getenv("GOOGLE_ID"),
		ClientSecret: os.Getenv("GOOGLE_SECRET"),
		RedirectURL:  "http://localhost:8080/google/redirect",
		Scopes:       []string{"email", "profile"},
		Endpoint:     google.Endpoint,
	}
}
