package web

// GoogleUser contains necessary fields returned from google api
type GoogleUser struct {
	ID    string
	Email string
	Name  string
}
