module web

require (
	cloud.google.com/go v0.34.0 // indirect
	github.com/davecgh/go-spew v1.1.1
	github.com/gobuffalo/buffalo-plugins v1.9.4 // indirect
	github.com/gorilla/sessions v1.1.3
	github.com/joho/godotenv v1.3.0
	github.com/julienschmidt/httprouter v1.2.0
	github.com/markbates/grift v1.0.5 // indirect
	github.com/markbates/refresh v1.4.11 // indirect
	github.com/ugorji/go/codec v0.0.0-20181209151446-772ced7fd4c2 // indirect
	golang.org/x/crypto v0.0.0-20190103213133-ff983b9c42bc // indirect
	golang.org/x/oauth2 v0.0.0-20181203162652-d668ce993890
	golang.org/x/tools v0.0.0-20190103205943-8a6051197512 // indirect
)
