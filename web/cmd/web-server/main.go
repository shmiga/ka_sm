package main

import (
	"fmt"
	"html/template"
	"log"
	netHttp "net/http"
	"os"
	"web"
	"web/http"

	"github.com/joho/godotenv"
	"github.com/julienschmidt/httprouter"
)

func main() {
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}

	r := httprouter.New()

	session := http.NewCookieSession([]byte(os.Getenv("COOKIE_SECRET")))

	renderer := http.NewRenderer(
		session,
		template.Must(template.ParseGlob("templates/*.gohtml")),
	)

	api := web.NewHTTP(os.Getenv("API_URL"))

	h := &http.Handler{renderer, api, session}

	r.GET("/", h.Index)
	r.GET("/login", h.GetLogin)
	r.POST("/refresh", h.RefreshEmail)
	r.POST("/login", h.PostLogin)
	r.GET("/logout", h.Logout)
	r.GET("/signup", h.GetSignup)
	r.POST("/signup", h.PostSignup)
	r.GET("/profile", h.GetProfile)
	r.POST("/profile", h.PostProfile)
	r.GET("/google/redirect", h.GoogleRedirect)
	r.GET("/google/sign", h.GoogleSign)
	r.NotFound = netHttp.FileServer(netHttp.Dir("assets/dist"))

	fmt.Println("web server running on port 8080")
	netHttp.ListenAndServe(":8080", r)
}
