export default class GoogleAutocomplete {
    constructor(el) {
        if (el) {
            this.autocomplete = new google.maps.places.Autocomplete(el, {types: ['geocode']});
        }
    }
}
