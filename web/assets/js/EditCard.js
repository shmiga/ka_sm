export default class EditCard {
    constructor() {
        this.profileCard = document.querySelector('.profile-card');
        this.editCard = document.querySelector('.edit');
        this.btn = document.querySelector('.button');
        this.init();
    }
    init() {
        this.btn.addEventListener('click', () => this.showEdit());
    }
    showEdit() {
        this.profileCard.style.display = 'none';
        this.editCard.style.display = 'block';
    }
}
