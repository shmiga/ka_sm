export default class Modal {
    constructor(el) {
        this.el = el;
    }
    show() {
        this.el.style.display = 'flex';
    }
    hide() {
        this.el.style.display = 'none';
    }
}
