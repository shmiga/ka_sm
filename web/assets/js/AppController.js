import FinishAccountController from './FinishAccountController';
import ProfileController from './ProfileController';
import LoginController from './LoginController';

export default class AppController {
    constructor() {
        this.location = window.location.pathname;
        this.init();
    }

    init() {
        switch(this.location) {
            case '/profile':
                new ProfileController;
                break;
            case '/signup':
                new FinishAccountController;
                break;
            case '/login':
                new LoginController;
                break;
        }
    }
}
