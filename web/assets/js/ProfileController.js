import GoogleAutcomplete from './GoogleAutocomplete';
import EditCard from './EditCard';

export default class ProfileController {
    constructor() {
        new GoogleAutcomplete(document.querySelector('#autocomplete'));
        new EditCard;
    }
}
