import Modal from './Modal';

export default class LoginController {
    constructor() {
        let el = document.querySelector('.refresh-modal');
        this.modal = new Modal(el);

        let btn = document.querySelector('.refresh-btn');
        btn.addEventListener('click', () => this.modal.show());
    }
}
