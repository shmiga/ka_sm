package web

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"

	"github.com/davecgh/go-spew/spew"
)

type APIService interface {
	GET(string) (map[string]interface{}, int, error)
	POST(string, map[string]interface{}) (map[string]interface{}, int, error)
	SetAuth(string) APIService
}

type HTTPApi struct {
	baseURL string
	token   string
}

func (a *HTTPApi) SetAuth(token string) APIService {
	a.token = token
	return a
}

func (a *HTTPApi) GET(path string) (map[string]interface{}, int, error) {
	client := &http.Client{}

	req, err := http.NewRequest("GET", a.baseURL+path, nil)
	if err != nil {
		return nil, 0, err
	}

	req.Header.Set("Authorization", "Bearer "+a.token)

	resp, err := client.Do(req)
	if err != nil {
		return nil, 0, err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, 0, err
	}

	spew.Dump(string(body))
	response := make(map[string]interface{})
	err = json.Unmarshal([]byte(body), &response)
	if err != nil {
		return nil, 0, err
	}

	return response, resp.StatusCode, nil
}

func (a *HTTPApi) POST(path string, data map[string]interface{}) (map[string]interface{}, int, error) {
	jsonData, err := json.Marshal(data)
	if err != nil {
		return nil, 0, err
	}

	client := &http.Client{}

	req, err := http.NewRequest("POST", a.baseURL+path, bytes.NewBuffer(jsonData))
	if err != nil {
		return nil, 0, err
	}

	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", "Bearer "+a.token)

	resp, err := client.Do(req)
	if err != nil {
		return nil, 0, err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, 0, err
	}

	response := make(map[string]interface{})
	err = json.Unmarshal([]byte(body), &response)
	if err != nil {
		return nil, 0, err
	}

	return response, resp.StatusCode, nil
}

func NewHTTP(baseURL string) APIService {
	return &HTTPApi{baseURL, ""}
}
